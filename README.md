# ITX8540

![intro-image](/uploads/e75fb784b43e68b70c94c15a504a28ff/intro-image.png)

# This is an entry point to our project.    

* The general information & description can be found in our [wiki](https://gitlab.com/masarov/ITX8540-docs/wikis/home)
* All the documents & meeting notes can be found [here](https://gitlab.com/masarov/ITX8540-docs/tree/master/docs)
* Back-end API repository is [here](https://gitlab.com/claantee/ITX8540-backendAPI)
* iOS application repository is [here](https://gitlab.com/avainola/ITX8540-iOS-App)
* Landing page & web app repository is [here](https://gitlab.com/masarov/ITX8540-web)
* Machine learning repositories:
    * Digital Clock Drawing Test repository is [here](https://gitlab.com/masarov/clock)
    * Error Detection Tool repository is [here](https://gitlab.com/konstantin.bardos/error-detection-tool)
* Issue tracker is [here](https://trello.com/b/pI4Rg7d9)  
