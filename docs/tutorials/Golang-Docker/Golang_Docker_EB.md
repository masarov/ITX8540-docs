## Go, Docker and Amazon Elastic Beanstalk

### Docker
Had to install Docker on my Mac https://docs.docker.com/docker-for-mac/


After installation tried starting a Dockerized webserver:  
	`docker run -d -p 80:80 --name webserver nginx`
	
	
Image was not found locally so it was pulled from Docker Hub.  
To see the the running web server details (-a to see all container):  
	`docker ps`


An image is a filesystem and parameters to use at runtime. It doesn’t have state and never changes.   
A container is a running instance of an image. When you run the command (`docker run hello-world`), Docker Engine:  
- checked to see if you had the hello-world software image  
- downloaded the image from the Docker Hub (more about the hub later)  
- loaded the image into the container and “ran” it  
Depending on how it was built, an image might run a simple, single command and then exit. This is what Hello-World did.


How to build my own image: https://docs.docker.com/engine/getstarted/step_four/  
In that tutorial was created a new image with a new Dockerfile which took some wise sayings from fortunes and ran the initial image with a parameter which included a random wise saying.


So far did:  
- installed Docker  
- run a software image in a container  
- located an interesting image on Docker Hub  
- run the image on your own machine  
- modified an image to create your own and run it  
- create a Docker Hub account and repository  
- pushed your image to Docker Hub for others to share  

## Go and Docker
Image: https://hub.docker.com/_/golang/


Created a Docker image for my Go API and tried to run it locally with Docker ✅  
Pushed the Go project image to my repo at Docker Hub ✅  
Removed local image and tried running the project from the repo ✅  


Pushing to repo:  
`docker tag IMAGE_ID USERNAME/IMAGE_NAME:latest`  
`docker push USERNAME/IMAGE_NAME`


Running the following command the image will be pulled from the repo and the Go code will be executed:  
`docker run --rm -it -p 8080:8080 caroliinalaantee/go-app`

## Deploying to Elastic Beanstalk
http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/go_docker_platform.html


Video: https://youtu.be/1zrtJWh0bRg (How to run a Go web app on Amazon's Elastic Beanstalk)


1. Rename Dockerfile to Dockerfile.local  
2. Create application source bundle, like this http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/applications-sourcebundle.html  
3. Create new application in Elastic Beanstalk following these steps http://docs.aws.amazon.com/elasticbeanstalk/latest/dg/applications.html  


