### 06.10.2016

How to make a virtualbox machine with ubuntu server 16.04.1 and install docker on it.

1.	Install virtualbox
2.	Download Ubuntu server 16.04.1 LTS from https://www.ubuntu.com/download/server 
3.	Create a new virtual machine for Ubuntu 64bit with default values
4.	Mount Ubuntu server image and installed it with default settings
5.	Ran the following commands in Ubuntu server:
$ sudo apt-get update 
$ sudo apt-get install apt-transport-https ca-certificates
$ sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
$ cd /etc/apt/sources.list.d
$ sudo vim docker.list
6.	Then used the vim editor to insert a new entry
Press i to edit the file
Add an entry: deb https://apt.dockerproject.org/repo ubuntu-xenial main
Press esc and ZZ to save and quit
7.	Then ran the following commands:
$ sudo apt-get update
$ sudo apt-get purge lxc-docker
$ apt-cache policy docker-engine
$ sudo apt-get update
$ sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
8.	Install Docker:
$ sudo apt-get update
$ sudo apt-get install docker-engine
$ sudo service docker start
$ sudo docker run hello-world
9.	If everything work you should see a message: Hello from docker�
