### 05.10.2016

We have:
* Discussed who did what during the last week
* Discussed the DB and Back-end solutions, ideas.
* Discussed the options/problems with hosting/server
* Created and assigned the tasks for the next sprint.
* Agreed to add tutorials folder in docs repo
