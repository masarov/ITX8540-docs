### 28.11.2016

Participants: Artur, Caroliina, Ilja, Konstantin, Sven Nõmm

- Martin is lost and we have to continue working on the web
    - For the next week divide the tasks between Caroliina and Jaak
- When drawing fast there are too few points and it is not tracking very well
    - Already have a solution for that
- The web side: should just make one page with data right now
    - Just a list of all the tests: patients, tests, results in three columns
    - Choose patient, choose test, show results (show the drawn image, parameters and graphs (radar chart for example))
    - The data shown should be informative and the doctor can print it out
- Should continue with machine learning although problems finishing it in time
    - In the end important to show that at least one machine learning algorithm is working
    - (IDEA) In the program there will be a rectangle around the object and prints out the value next to it
    