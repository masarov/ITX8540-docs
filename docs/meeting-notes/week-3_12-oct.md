### 12.10.2016

Participants: Ilja, Caroliina, Jaak, Artur, Konstantin, Martin, Sven Nõmm

* Drawing is possible and sending data to the database using AWS API gateway + Amazon DynamoDB
* Set up Go project (MongoDB) with Docker (also possible to send data to the database)
* Discussed using AWS gateway with DynamoDB at the same time with Docker, Go and MongoDB
* Maybe should use push notifications

Next steps:
* Continue configuring back-end, set it up on server
* Continue developing the iOS application

What Sven Nõmm pointed out:
* Use - "Fine motoric tests" (peenmotoorika testid)
* In most of the cases the app will be used by nurses. We need to think about some kind of training process for them. The manual should be understandable.
* We should have some part (mechanism), that controls that the data is correct.
* We probably need to show some data to the doctor after the patient has performed the test (motion parameters, some predictions, comparison with the average, etc.)
