### 19.10.2016

Participants: Ilja, Caroliina, Jaak, Artur, Konstantin, Martin, Sven Nõmm

* We have to create a document that includes the description of what every person is going to deal with (specific tasks) and how the responsibility areas are divided.
Sven is going to grade us every week depending on the work (commits) that we have done in the week.

---

* Doctor would like to know immediately how the patient compares to the average results of other people (should have a different report page for that or show the results in iPad as statistics)
* Analyze how well does the patient differ from the average regarding different parameters
* Think when to update the database which we use for descriptive statistics purposes
* Doctors want to know why a certain decision was made
