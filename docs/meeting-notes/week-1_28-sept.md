### 28.09.2016

We have:
* Discussed the project in general
* Discussed the initial scope for the MVP
* Discussed technical solution, roles, roadmap
* Selected the VCS, Wiki and issue tracker. (GitLab & Trello)
* Created the tasks in Trello for the first weekly sprint
* Assigned the tasks
