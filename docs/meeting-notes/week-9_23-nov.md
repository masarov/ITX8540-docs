### 23.11.2016

Participants: Caroliina, Jaak, Konstantin, Artur, Sven Nõmm

- Discussing what someone has done and will do for the next weeks
- Testing and fixing
- Where's Martin?
- How to go forward with the web side (showing different plots)?
- Discussing taking part of the Startup competition in Mektory
- Setting up the web part on Amazon
- Web is important that the doctor is able to print it out
    1. Should include the drawn image
    2. Should include the data with graphs
    