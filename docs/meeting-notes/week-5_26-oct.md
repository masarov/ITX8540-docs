### 26.10.2016

Participants: Ilja, Caroliina, Jaak, Artur, Konstantin, Martin, Sven Nõmm

- Everyone needs to write a list of tasks that he/she needs to do in the few following weeks. Put it all together (Excel sheet for example).
- Define, what API Endpoints we need exactly for communicating with the database. How to divide our data between different databases?
- What graphs to use for visualizing?
- Next weeks goal: create a prototype that is able to give back some data
    - Test is finished --> Get top 10(?) parameters for that particular test --> Calculate how the person differs compared to the average (standard deviation) 
of those parameters and create some visualizations --> Display the results/visualization to the doctor
