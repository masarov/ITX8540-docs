## Research Papers analysis summary

### Description:

The Clock Drawing Test—a simple pencil and paper test—has been used for more than 50 years as a screening tool to differentiate normal individuals from those with cognitive impairment, and has proven useful in helping to diagnose cognitive dysfunction associated with neurological disorders such as Alzheimer’s disease, Parkinson’s disease, and other dementias and conditions.

Research is underway on many fronts, including pharmacological treatment, but there is as yet no cure for cognitive impairments such as Alzheimer’s disease (AD) and Parkinson’s disease (PD). There is however the potential to slow the progress of some forms of cognitive decline, if caught early enough. Hence one important focus of research is **early detection**.

### Key moments in research papers

2 types of test:

* “Command clock” - draw (Challenges things like language and memory)
* “Copy clock” - copy (Challenges spatial planning and executive function - the ability to plan and organize)

CDT is used by neuropsychologists, neurologists and primary care physicians as part of a general screening for cognitive change

Most digital tests use "Anoto, Inc" ballboint pen for performing the test. (This pen records only coordinates with timestamp, without pressure)

Automated versions of 7+ existing scoring systems for Clock test.

Algorithm classifies the pen strokes as clock drawing symbols + might be corrected by human scorer (which means that error rate is quite high?).

The most important **features**:

* Number of strokes
* Total ink length
* Total time to draw
* Pen speed for different components
* Latencies between components
* Length of the major and minor axis and eccentricity of the fitted ellipse
* Largest angular gaps in the clock face
* Distance and angular difference between starting and ending points of the clock face
* Digits that are missing or repeated
* Height and width of digit bounding boxes
* Omissions or repetitions of hands
* Angular error from their correct angle
* The hour to minute hand size ratio
* The presence and direction of arrowheads
* Ink marks

Some papers claim to have **500+ features** extracted total.

Machine learning used for classifiers:

* CART
* SVM
* Random forests
* Boosted decision trees
* Logistic regression

### Problems:

* Overwritten/crossed-out digits recognition
* Digit recognition score (~96% - which is not that good)


### Possible gaps in research:

* Pressure is not used
* Azimuth and Latitude angles are not used
* No use of deep learning algorithms
